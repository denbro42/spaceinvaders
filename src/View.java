package spaceinvaders.src;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

import spaceinvaders.src.objects.*;
import spaceinvaders.src.models.*;
import spaceinvaders.src.views.*;

/**
 * View.java
 * @author Denis Griffis
 *
 * Responsible for all graphical display of the application.  This is
 * controlled by the SpaceInvaders controller class, and contains references to
 * relevant models.
 *
 * @see SpaceInvaders.java
 * @see SpaceInvadersModel.java
 * @see HighScoresModel.java
 * @see Screen.java
 * @see WelcomeScreen.java
 * @see GameScreen.java
 * @see HighScoresScreen.java
 * @see animators/
 */
public class View {

    private int width;
    private int height;
    private Color backgroundColor = Color.black;
    private JFrame mainframe;
    private Screen contents;

    private SpaceInvadersModel gameModel;
    private HighScoresModel scoresModel;

    private int state;
    public static int WELCOME = 0;
    public static int GAME = 1;
    public static int HIGHSCORES = 2;


    /**
     * Initialize a View object in a particular state; the input state is
     * expected to be one of the static class variables WELCOME, GAME, or
     * HIGHSCORES
     * @param int state The state the View should be initialized to
     */
    public View(int state, int w, int h, SpaceInvadersModel game,
                HighScoresModel scores) {
        this.width = w;
        this.height = h;

        // create the main JFrame that will display all content, and set
        // persistent global settings
        this.mainframe = new JFrame();
        this.mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.mainframe.setResizable(false);

        this.setScreen(state);
        this.mainframe.setVisible(true);

        this.gameModel = game;
        this.scoresModel = scores;
    }


    /**
     * Change the screen displayed by a View; this can be either the welcome
     * screen, the game screen, or the high scores screen
     * @param int state One of class vars WELCOME, GAME, or HIGHSCORES,
     *                  representing the modal screen to display
     */
    public void setScreen(int state) {
        if (state == View.WELCOME) {
            this.contents = new WelcomeScreen(new Dimension(this.width,
                                                            this.height),
                                              this.backgroundColor);
        } else if (state == View.GAME) {
            this.contents = new GameScreen(new Dimension(this.width,
                                                         this.height),
                                           this.backgroundColor);
        } else if (state == View.HIGHSCORES) {
            this.contents = new HighScoresScreen(new Dimension(this.width,
                                                               this.height),
                                                 this.backgroundColor);
        }

        // change the state AFTER initializing the new content to avoid issues
        // with the control thread attempting to render a JPanel that hasn't
        // completed its initialization thread
        this.state = state;

        // wipe out whatever the frame was displaying and push in the new
        // game mode screen
        this.mainframe.getContentPane().removeAll();
        this.mainframe.getContentPane().add(this.contents);

        this.mainframe.pack();
    }


    /**
     * Update the contents of the screen displayed to reflect the state of the
     * relevant model, and then redraw the screen
     */
    public void update() {
        if (this.state == View.GAME) {
            ArrayList<GameObject> updatedObjects = this.gameModel.getState();
            GameScreen screen = (GameScreen)this.contents;
            screen.updateObjects(updatedObjects);
            screen.displayScore(this.gameModel.getScore());
        } else if (this.state == View.HIGHSCORES) {
            Score[] updatedScores = this.scoresModel.getHighScores();
            HighScoresScreen screen = (HighScoresScreen)this.contents;
            screen.updateHighScores(updatedScores);
        }
        this.contents.repaint();
    }


    public void addKeyListener(KeyListener kl) {
        this.mainframe.addKeyListener(kl);
    }


    // used for displaying "You won!" and "Game over" messages
    public void displayBigMessage(String message) {
        this.contents.displayBigMessage(message);
    }
    public void resetBigMessage() {
        this.contents.resetBigMessage();
    }
}
