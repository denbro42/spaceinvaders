package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;


/**
 * TankAnimator.java
 * @author Denis Griffis
 *
 * Renderer for Tank objects
 */
public class TankAnimator extends Animator {

    private static String[] mainAnimationFrames =
                                new String[] {"/imgs/tank.jpg"};
    private static String[] explosionFrames =
                                new String[] {"/imgs/tank_explode.jpg"};

    public TankAnimator(GameObject observer) {
        super(TankAnimator.mainAnimationFrames, observer);
    }

    public void explode() {
        this.setAnimation(TankAnimator.explosionFrames);
    }

}
