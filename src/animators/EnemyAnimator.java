package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.objects.active.Enemy;


/**
 * EnemyAnimator.java
 * @author Denis Griffis
 *
 * Renderer for Enemy objects
 */
public class EnemyAnimator extends Animator {

    private static String[] droneMainAnimationFrames =
                                new String[] {"/imgs/drone_1.jpg",
                                              "/imgs/drone_2.jpg"};
    private static String[] hunterMainAnimationFrames =
                                new String[] {"/imgs/hunter_1.jpg",
                                              "/imgs/hunter_2.jpg"};
    private static String[] squidMainAnimationFrames =
                                new String[] {"/imgs/squid_1.jpg",
                                              "/imgs/squid_2.jpg"};

    private static int droneStepDelay = 30;
    private static int hunterStepDelay = 25;
    private static int squidStepDelay = 20;

    private static String[] explosionFrames =
                                new String[] {"/imgs/enemy_explode.jpg"};

    public EnemyAnimator(GameObject observer, int type) {
        // initialize the parent with drone frames as default to avoid
        // nullPointerExceptions
        super(EnemyAnimator.droneMainAnimationFrames, observer, 0);
        // now adjust to the correct frames
        if (type == Enemy.DRONE) {
            this.setStepDelay(EnemyAnimator.droneStepDelay);
        } else if (type == Enemy.HUNTER) {
            this.setAnimation(EnemyAnimator.hunterMainAnimationFrames);
            this.setStepDelay(EnemyAnimator.hunterStepDelay);
        } else if (type == Enemy.SQUID) {
            this.setAnimation(EnemyAnimator.squidMainAnimationFrames);
            this.setStepDelay(EnemyAnimator.squidStepDelay);
        }
    }

    public void explode() {
        this.setAnimation(this.explosionFrames);
    }

}
