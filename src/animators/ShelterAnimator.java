package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;


/**
 * ShelterAnimator.java
 * @author Denis Griffis
 *
 * Renderer for Shelter objects
 */
public class ShelterAnimator extends Animator {
    private static String[] noDamageFrames =
                                new String[] {"/imgs/shelter_1.jpg"};
    private static String[] damage1Frames =
                                new String[] {"/imgs/shelter_2.jpg"};
    private static String[] damage2Frames =
                                new String[] {"/imgs/shelter_3.jpg"};

    public ShelterAnimator(GameObject observer) {
        super(ShelterAnimator.noDamageFrames, observer);
    }

    public void showDamage(int damage) {
        switch (damage) {
            case 1:
                this.setAnimation(ShelterAnimator.damage1Frames);
                break;
            case 2:
                this.setAnimation(ShelterAnimator.damage2Frames);
                break;
        }
    }

}
