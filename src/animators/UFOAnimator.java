package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;


/**
 * UFOAnimator.java
 * @author Denis Griffis
 *
 * Renderer for UFO objects
 */
public class UFOAnimator extends Animator {

    private static String[] mainAnimationFrames =
                                new String[] {"/imgs/ufo_1.jpg",
                                              "/imgs/ufo_2.jpg"};
    private static String[] explosionFrames =
                                new String[] {"/imgs/enemy_explode.jpg"};

    public UFOAnimator(GameObject observer) {
        super(UFOAnimator.mainAnimationFrames, observer, 20);
    }

    public void explode() {
        this.setAnimation(this.explosionFrames);
    }

}
