package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;

/**
 * GroundAnimator.java
 * @author Denis Griffis
 *
 * Renderer for Ground objects
 */
public class GroundAnimator extends Animator {

    private static String[] mainAnimationFrames =
                                new String[] {"/imgs/ground.jpg"};

    public GroundAnimator(GameObject observer) {
        super(GroundAnimator.mainAnimationFrames, observer);
    }

}
