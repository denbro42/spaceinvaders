package spaceinvaders.src.animators;

import javax.swing.*;
import java.awt.*;

import spaceinvaders.src.objects.GameObject;


/**
 * Animator.java
 * @author Denis Griffis
 *
 * Abstract implementation of a controller for animating game objects;
 * responsible for retrieving image files, cycling between them to animate, and
 * updating which animation is currently being run
 *
 * @see View.java
 */
public abstract class Animator {

    protected Sprite cursprite;
    // used to mark how many game steps to wait before cycling animation frames
    protected int stepDelay = 0;
    protected int stepCount = 0;

    // the game object being animated needs to know when the animation is
    // changed to update its dimensions
    protected GameObject observer;

    /**
     * Initialize an Animator object with a set of files representing the
     * default animation and a game object observing animation changes
     *
     * @param String[] defaultAnimationFiles A list of resource locators for
     *                                       the image files representing the
     *                                       default animation
     * @param GameObject observer The object being animated
     */
    public Animator(String[] defaultAnimationFiles, GameObject obs) {
        this.observer = obs;
        this.stepDelay = 20;
        this.setAnimation(defaultAnimationFiles);
    }
    /**
     * Adds the optional stepDelay param
     * @param int delay How many game steps to wait between cycling frames
     */
    public Animator(String[] defaultAnimationFiles, GameObject obs,
                    int delay) {
        this.observer = obs;
        this.setStepDelay(delay);
        this.setAnimation(defaultAnimationFiles);
    }


    /**
     * Update the animation currently being run
     * @param String[] animationFiles List of resource locators for the new
     *                                animation's frames
     */
    public void setAnimation(String[] animationFiles) {
        this.cursprite = new Sprite(animationFiles);
    }

    
    /**
     * If the proper number of game steps have been waited out, cycles the
     * current animation's frame
     */
    public void frameStep() {
        this.stepCount++;
        if (this.stepCount >= this.stepDelay) {
            this.stepCount = 0;
            this.cursprite.frameStep();
        }
    }

    
    /**
     * Render the current animation frame
     */
    public void draw(Graphics g, int posX, int posY, JPanel observer) {
        this.cursprite.draw(g, posX, posY, observer);
    }


    // getters and setters
    public int getCurrentWidth() { return this.cursprite.getWidth(); }
    public int getCurrentHeight() { return this.cursprite.getHeight(); }
    public void setStepDelay(int delay) {
        this.stepDelay = delay;
    }

}
