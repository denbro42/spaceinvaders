package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;

/**
 * ScreenEdgeAnimator.java
 * @author Denis Griffis
 *
 * Renderer for ScreenEdge objects
 */
public class ScreenEdgeAnimator extends Animator {

    private static String[] mainAnimationFrames =
                                new String[] {"/imgs/screenblock.jpg"};

    public ScreenEdgeAnimator(GameObject observer) {
        super(ScreenEdgeAnimator.mainAnimationFrames, observer);
    }

}
