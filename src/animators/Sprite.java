package spaceinvaders.src.animators;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import javax.imageio.*;
import java.awt.image.*;
import java.io.*;

/**
 * Sprite.java
 * @author Denis Griffis
 *
 * Provides functionality for retrieving and rendering an image resource
 *
 * @see Animator.java
 */
public class Sprite {

    private int width;
    private int height;

    private int curframe;
    private ArrayList<BufferedImage> frames;

    public Sprite(String[] imgresources) {
        frames = new ArrayList<BufferedImage>();

        BufferedImage frame = null;
        for (int i=0; i < imgresources.length; i++) {
            frame = null;
            try {
                frame = ImageIO.read(Sprite.class.getResource(
                                                        imgresources[i]));
            } catch (IOException e) { }
            this.frames.add(frame);
        }

        this.height = frame.getHeight();
        this.width = frame.getWidth();

        this.curframe = 0;
    }

    public void draw(Graphics g, int posX, int posY, JPanel observer) {
        BufferedImage frame = this.frames.get(this.curframe);
        g.drawImage(frame, posX, posY, observer);
    }

    public void frameStep() {
        this.curframe++;
        if (this.curframe >= this.frames.size()) {
            this.curframe = 0;
        }
    }

    public int getWidth() { return this.width; }
    public int getHeight() { return this.height; }

}
