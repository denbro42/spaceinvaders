package spaceinvaders.src.animators;

import spaceinvaders.src.objects.GameObject;


/**
 * BulletAnimator.java
 * @author Denis Griffis
 *
 * Renderer for Bullet objects
 */
public class BulletAnimator extends Animator {

    private static String[] tankMainAnimationFrames =
                                new String[] {"/imgs/bullet.jpg"};
    private static String[] enemyMainAnimationFrames =
                                new String[] {"/imgs/enemy_bullet.jpg"};

    public BulletAnimator(GameObject observer, boolean playerBullet) {
        super(BulletAnimator.tankMainAnimationFrames, observer);
        if (! playerBullet) {
            this.setAnimation(BulletAnimator.enemyMainAnimationFrames);
        }
    }

}
