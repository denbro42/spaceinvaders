package spaceinvaders.src.models;

/**
 * Velocity.java
 * @author Denis Griffis
 *
 * Minor data structure used for readability in a few places; allows for
 * storage of semantically-paired ints representing 2-d velocity
 */
public class Velocity {
    
    public int vx;
    public int vy;

    public Velocity(int x, int y) {
        this.vx = x;
        this.vy = y;
    }
}
