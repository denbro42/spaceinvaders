package spaceinvaders.src.models;

import java.util.*;
import java.io.*;


/**
 * HighScoresModel.java
 * @author Denis Griffis
 *
 * Responsible for all file I/O operations for high scores, including adding
 * the player's score into the high scores and saving that to a persistent
 * file.  Note that if the high scores file is not found or malformed, it will
 * be erased and a default scores file put in its place.
 *
 * @see SpaceInvaders.java
 * @see HighScoresScreen.java
 */
public class HighScoresModel {

    private static String scoresPath = "bin/highscores.txt";
    private static int numHighScores = 5;

    private Score[] scores;

    private Score playerScore;
    private int playerScoreIndex;
    private boolean recording;
    private char[] playerInitials;
    private int currentInitial;

    public HighScoresModel() {
        try { this.fetchHighScores(); }
        catch (IOException e) {}

        this.playerScore = null;
        this.recording = false;
    }


    /**
     * Flag that the model is expecting the player to enter their initials, and
     * set up a new score entry for them to save
     * @param int score The player's score to save
     */
    public void askForInitials(int score) {
        this.playerScore = new Score("___", score);
        this.insertScore(this.playerScore, this.playerScoreIndex);
        this.recording = true;
        this.playerInitials = new char[]{'_','_','_'};
        this.currentInitial = 0;
    }


    /**
     * Insert a score into the recorded high scores and shift all lower scores
     * down
     * @param Score score The Score object to insert
     * @param int index The index in the internal scores list to insert it
     */
    private void insertScore(Score score, int index) {
        for (int i=HighScoresModel.numHighScores-2; i>=index; i--) {
            this.scores[i+1] = this.scores[i];
        }
        this.scores[index] = score;
    }


    /**
     * Mark the letter the user input as the next initial and set up recording
     * the following initial; if all 3 have been entered, flushes the high
     * scores to the recorded data file and stops recording
     * @param char letter The initial entered by the user
     */
    public void recordLetter(char letter) {
        this.playerInitials[this.currentInitial] = letter;
        this.playerScore.flushInitials(this.playerInitials);
        this.currentInitial++;
        if (this.currentInitial >= 3) {
            this.recording = false;
            this.writeHighScores();
        }
    }

    public boolean isRecording() {
        return this.recording;
    }



    /**
     * Check if a score is high enough to be included in the high scores
     */
    public boolean isItHigh(int score) {
        for (int i=0; i<this.scores.length; i++) {
            Score nextScore = this.scores[i];
            if (score > nextScore.score) {
                this.playerScoreIndex = i;
                return true;
            }
        }
        return false;
    }


    /**
     * Fetches the high scores from the high scores file, with very strict
     * format expectations
     * @return Score[] The 5-length array of the recorded high scores
     */
    private Score[] fetchHighScores() throws IOException {
        File scoresFile = new File(HighScoresModel.scoresPath);
        if (! scoresFile.exists()) {
            this.writeDefaultHighScores();
            return this.fetchHighScores();
        }


        Scanner scanner = new Scanner(scoresFile);

        // parse the scoresFile into an array of 5 high scores extremely
        // strictly; if any errors occur, then the default scores are written
        // to the file and returned
        this.scores = new Score[HighScoresModel.numHighScores];
        int scoreCount = 0;
        while (scanner.hasNextLine() &&
                scoreCount<HighScoresModel.numHighScores) {
            String line = scanner.nextLine();
            Scanner lineScanner = new Scanner(line);

            if (!lineScanner.hasNext()) {
                this.writeDefaultHighScores();
                return this.fetchHighScores();
            }
            String player = lineScanner.next();
            if (!lineScanner.hasNextInt()) {
                this.writeDefaultHighScores();
                return this.fetchHighScores();
            }
            int score = lineScanner.nextInt();

            Score scoreObj = new Score(player, score);
            this.scores[scoreCount] = scoreObj;

            scoreCount++;
        }

        // if there are fewer than 5 high scores in the file, consider it
        // corrupted and flush the defaults
        if (scoreCount < 5) {
            this.writeDefaultHighScores();
            return this.fetchHighScores();
        }

        return new Score[]{};
    }


    /**
     * Write the current scores in the model out to the high scores file
     */
    private void writeHighScores() {
        FileOutputStream scoresOutput;
        try {
            scoresOutput = new FileOutputStream(HighScoresModel.scoresPath);
        } catch (FileNotFoundException e) { return; }

        PrintStream output = new PrintStream(scoresOutput);
        for (int i=0; i<5; i++) {
            Score score = this.scores[i];
            output.println(score.player+" "+score.score);
        }

        output.close();
        try {
            scoresOutput.close();
        } catch (IOException e) { }
    }


    /**
     * Flush default high scores into the high scores file
     */
    private void writeDefaultHighScores() {
        this.scores = new Score[] {
            new Score("UGH", 50000),
            new Score("STE", 40000),
            new Score("JON", 30000),
            new Score("HAM", 20000),
            new Score("LND", 10000)
        };
        this.writeHighScores();
    }


    public Score[] getHighScores() {
        return this.scores;
    }

}
