package spaceinvaders.src.models;

/**
 * Score.java
 * @author Denis Griffis
 *
 * Minor data structure used for readability; contains a player's initials and
 * a score corresponding to those initials
 */
public class Score {

    public String player;
    public int score;

    public Score(String p, int s) {
        this.player = p;
        this.score = s;
    }

    public void flushInitials(char[] chars) {
        this.player = new String(chars);
    }

}
