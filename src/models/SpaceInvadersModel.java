package spaceinvaders.src.models;

import java.util.*;

import spaceinvaders.src.objects.*;
import spaceinvaders.src.objects.active.*;
import spaceinvaders.src.objects.stationary.*;


/**
 * SpaceInvadersModel.java
 * @author Denis Griffis
 *
 * Responsible for storing and manipulating the data representing the state of
 * the Space Invaders game
 *
 * @see SpaceInvaders.java
 * @see GameScreen.java
 * @see objects/
 */
public class SpaceInvadersModel {

    // all the internal object (set) pointers
    private ArrayList<GameObject> objects;
    private ArrayList<ActiveObject> activeObjects;
    private ArrayList<GameObject> markedObjects;
    private ArrayList<Enemy> enemies;
    private ArrayList<Enemy> drones;
    private ArrayList<Enemy> hunters;
    private ArrayList<Enemy> squids;
    private Tank tank;
    private ScreenEdge leftEdge;
    private ScreenEdge rightEdge;

    private int worldWidth;
    private int worldHeight;

    private int score = 0;

    // global game states
    private boolean paused;
    private boolean gameComplete;
    private boolean playerWon;
    private boolean invasionSuccessful;

    // variables for handling shifting enemies down towards the player at timed
    // intervals
    private int enemyShiftDownDelay = 400;
    private int enemyShiftDownCount;
    private int enemyShiftingLimit = 10;
    private int enemyShiftingCount;
    private boolean enemyShifting;

    // reversal flags to ensure that group does not reverse more than once per
    // game step
    private boolean squidsReversed = false;
    private boolean huntersReversed = false;
    private boolean dronesReversed = false;


    /**
     * Initialize the SpaceInvadersModel to default contents
     * @param int width The width of the game world
     * @param int height The height of the game world
     */
    public SpaceInvadersModel(int width, int height) {
        // init all the lists of object references
        this.objects = new ArrayList<GameObject>();
        this.activeObjects = new ArrayList<ActiveObject>();
        this.markedObjects = new ArrayList<GameObject>();
        this.enemies = new ArrayList<Enemy>();
        this.drones = new ArrayList<Enemy>();
        this.hunters = new ArrayList<Enemy>();
        this.squids = new ArrayList<Enemy>();

        this.worldWidth = width;
        this.worldHeight = height;

        this.reinitialize();
    }


    /**
     * Return the SpaceInvadersModel to the initial state; all enemies, no
     * bullets, default locations, etc;
     */
    public void reinitialize() {
        // erase any leftover objects from previous runs
        this.objects.clear();
        this.activeObjects.clear();
        this.markedObjects.clear();
        this.enemies.clear();
        this.drones.clear();
        this.hunters.clear();
        this.squids.clear();

        this.gameComplete = false;
        this.playerWon = false;
        this.invasionSuccessful = false;
        this.paused = false;
        this.enemyShiftDownCount = 0;
        this.enemyShiftingCount = 0;
        this.enemyShifting = false;

        // create the borders of the world
        ScreenEdge leftEdge = new ScreenEdge(ScreenEdge.LEFT, this.worldWidth,
                                             this.worldHeight);
        this.leftEdge = leftEdge;
        ScreenEdge rightEdge = new ScreenEdge(ScreenEdge.RIGHT, this.worldWidth,
                                              this.worldHeight);
        this.rightEdge = rightEdge;
        ScreenEdge topEdge = new ScreenEdge(ScreenEdge.TOP, this.worldWidth,
                                            this.worldHeight);
        this.addGameObject(leftEdge);
        this.addGameObject(rightEdge);
        this.addGameObject(topEdge);

        // create static contents
        Ground ground = new Ground(0,530);
        this.addGameObject(ground);
        Shelter leftShelter = new Shelter(110, 420);
        Shelter middleShelter = new Shelter(360, 420);
        Shelter rightShelter = new Shelter(600, 420);
        this.addGameObject(leftShelter);
        this.addGameObject(middleShelter);
        this.addGameObject(rightShelter);

        // and re-create dynamic contents in default locations
        Tank tank = new Tank(368,490,0,0);
        this.addGameObject(tank);
        for (int i=0; i<8; i++) {
            // squid row
            Enemy squid = new Enemy(100 + 75*i, 100, -5, 0, Enemy.SQUID);
            this.addGameObject(squid);
            // hunter rows
            Enemy hunter = new Enemy(100 + 75*i, 150, -5, 0, Enemy.HUNTER);
            this.addGameObject(hunter);
            hunter = new Enemy(100 + 75*i, 200, -5, 0, Enemy.HUNTER);
            this.addGameObject(hunter);
            // drone rows
            Enemy drone = new Enemy(100 + 75*i, 250, -5, 0, Enemy.DRONE);
            this.addGameObject(drone);
            drone = new Enemy(100 + 75*i, 300, -5, 0, Enemy.DRONE);
            this.addGameObject(drone);
        }
    }

    
    /**
     * Add a game object of any type to the model; adds a reference to the
     * object to the relevant internal pointer arrays
     * @param GameObject obj The Game Object subclass instance to be added
     */
    public void addGameObject(GameObject obj) {
        this.objects.add(obj);
        if (obj instanceof ActiveObject) {
            ActiveObject activeObj = (ActiveObject)obj;
            this.activeObjects.add(activeObj);
        }

        if (obj instanceof Enemy) {
            Enemy en = (Enemy)obj;
            this.enemies.add(en);

            // and add it to the type-specific array (for per-type control)
            int type = en.getType();
            if (type == Enemy.DRONE) {
                this.drones.add(en);
            } else if (type == Enemy.HUNTER) {
                this.hunters.add(en);
            } else if (type == Enemy.SQUID) {
                this.squids.add(en);
            }
        } else if (obj instanceof Tank) {
            this.tank = (Tank)obj;
        }
    }


    /**
     * Remove a game object of any type from the model; removes all relevant
     * internal references to the object; also handles termination conditions
     * @param GameObject obj The Game Object instance to be deleted
     */
    private void deleteGameObject(GameObject obj) {
        this.objects.remove(obj);
        if (obj.isDying()) {
            this.markedObjects.remove(obj);
        }
        if (obj instanceof ActiveObject) {
            ActiveObject activeObj = (ActiveObject)obj;
            this.activeObjects.remove(activeObj);
        }

        if (obj instanceof Enemy) {
            Enemy en = (Enemy)obj;
            this.enemies.remove(en);

            // and remove it from the type-specific array
            int type = en.getType();
            if (type == Enemy.DRONE) {
                this.drones.remove(en);
            } else if (type == Enemy.HUNTER) {
                this.hunters.remove(en);
            } else if (type == Enemy.SQUID) {
                this.squids.remove(en);
            }
        }
    }


    /**
     * Update the positions and state of all objects in the model
     */
    public void step() {
        // don't make any changes if the game is paused
        if (this.isPaused()) { return; }

        // reset flags making sure that group reversal does not occur multiple
        // times per step
        this.dronesReversed = false;
        this.huntersReversed = false;
        this.squidsReversed = false;

        // handle shifting enemies down as necessary
        if (! this.enemyShifting) {
            this.enemyShiftDownCount++;
            if (this.enemyShiftDownCount >= this.enemyShiftDownDelay) {
                this.enemyShiftDownCount = 0;
                this.enemyShiftingCount = 0;
                this.enemyShifting = true;
                for (int i=0; i<this.enemies.size(); i++) {
                    Enemy en = this.enemies.get(i);
                    en.startShiftingDown();
                }
            }
        } else {
            this.enemyShiftingCount++;
            if (this.enemyShiftingCount == this.enemyShiftingLimit) {
                this.enemyShiftingCount = 0;
                this.enemyShifting = false;
                for (int i=0; i<this.enemies.size(); i++) {
                    Enemy en = this.enemies.get(i);
                    en.stopShiftingDown();
                }
            }
        }

        // move all objects
        for (int i=0; i<this.objects.size(); i++) {
            GameObject obj = this.objects.get(i);
            obj.step();
        }

        // kill those that need killing
        for (int i=0; i<this.markedObjects.size(); i++) {
            GameObject obj = this.markedObjects.get(i);
            if (obj.readyToDie()) {
                this.deleteGameObject(obj);
            }
        }

        // randomly spawn enemy bullets
        Random rand = new Random();
        for (int i=0; i<this.enemies.size(); i++) {
            Enemy enemy = this.enemies.get(i);
            // adjust likelihood of shooting based on number of enemies
            // remaining in the game
            if (rand.nextFloat() < (0.01 - .00025*this.enemies.size())) {
                Bullet bullet = enemy.shoot();
                this.addGameObject(bullet);
            }
        }

        // randomly spawn ufos
        if (rand.nextFloat() < 0.01) {
            UFO ufo = new UFO(5, 50, 8, 0);
            this.addGameObject(ufo);
        }

        this.collisionCheck();

        this.checkVictoryConditions();
    }


    /**
     * Loop through all current ActiveObjects and see if any are currently
     * colliding with any other objects
     */
    public void collisionCheck() {
        GameObject a, b;
        for (int i = 0; i < this.activeObjects.size(); i++) {
            a = this.activeObjects.get(i);
            for (int j=0; j<this.objects.size(); j++) {
                b = this.objects.get(j);
                // skip collision-checking the object with itself
                if (a == b) { continue; }
                if (this.colliding(a, b)) {
                    //System.out.println("collision occurred: " +
                    //    a.getClass().getName()+" => "+
                    //    b.getClass().getName());
                    int aAction = a.collision(b);
                    int bAction = b.collision(a);

                    this.takeCollisionAction(aAction, a);
                    this.takeCollisionAction(bAction, b);
                }
            }
        }
    }


    /**
     * Returns if two objects are colliding (overlapping the same space)
     * @param GameObject a,b The two objects being checked for a collision
     */
    private boolean colliding(GameObject a, GameObject b) {
        boolean hcollide = ((a.getX() <= b.getX() + b.getWidth()
                             && a.getX() >= b.getX())
                            || (a.getX() + a.getWidth() <= b.getX()
                                                           + b.getWidth()
                                && a.getX() + a.getWidth() >= b.getX()));
        boolean vcollide = ((a.getY() <= b.getY() + b.getHeight()
                             && a.getY() >= b.getY())
                            || (a.getY() + a.getHeight() <= b.getY()
                                                           + b.getHeight()
                                && a.getY() + a.getHeight() >= b.getY()));
        return (hcollide && vcollide);
    }


    /**
     * Modify the model to reflect the action an object said to take on its
     * collision with something else
     * @param int action The GameObject static variable representing what
     *                   action to take in the model
     * @param GameObject obj The GameObject instance that requested the action
     */
    private void takeCollisionAction(int action, GameObject obj) {
        if (action == GameObject.DELETE) {
            this.deleteGameObject(obj);
        }
        
        else if (action == GameObject.MARKFORDEATH) {
            this.markedObjects.add(obj);
            obj.markForDeath();
            // and add to the score if an enemy got shot
            if (obj instanceof Enemy) {
                Enemy en = (Enemy)obj;
                this.score += en.getScore();
            } else if (obj instanceof UFO) {
                UFO ufo = (UFO)obj;
                this.score += ufo.getScore();
            }
        }

        else if (action == GameObject.REVERSE) {
            this.reverseEnemyGroup(obj);
        }

        else if (action == GameObject.GAMELOST) {
            this.invasionSuccessful = true;
        }
    }


    /**
     * Reverse horizontal direction of an entire type-grouped set of enemies
     */
    private void reverseEnemyGroup(GameObject obj) {
        if (! (obj instanceof Enemy)) { return; }
        Enemy en = (Enemy)obj;

        if (en.getType() == Enemy.DRONE) {
            if (this.dronesReversed) { return; }
            else { this.dronesReversed = true; }
            for (int i=0; i<this.drones.size(); i++) {
                Enemy drone = this.drones.get(i);
                drone.reverse();
            }
        } else if (en.getType() == Enemy.HUNTER) {
            if (this.huntersReversed) { return; }
            else { this.huntersReversed = true; }
            for (int j=0; j<this.hunters.size(); j++) {
                Enemy hunter = this.hunters.get(j);
                hunter.reverse();
            }
        } else if (en.getType() == Enemy.SQUID) {
            if (this.squidsReversed) { return; }
            else { this.squidsReversed = true; }
            for (int k=0; k<this.squids.size(); k++) {
                Enemy squid = this.squids.get(k);
                squid.reverse();
            }
        }
    }


    /**
     * Check if the game has ended; if so, mark it appropriately
     */
    private void checkVictoryConditions() {
        // check if the game has been won
        if (this.drones.isEmpty()
                && this.hunters.isEmpty()
                && this.squids.isEmpty()) {
            this.gameWon();
        } else if (this.tank.isDying()
                   || this.invasionSuccessful) {
            this.gameOver();
        }
    }


    /**
     * Update the tank's velocity based on human player input
     * @param Velocity v The velocity pair indicating how to move the tank
     */
    public void moveTank(Velocity v) {
        // if the tank has exploded, don't move it
        if (this.tank.isDying()) {
            this.tank.setVelocity(0,0);
            return;
        }
        // otherwise, move it anywhere other than into the edges of the screen
        if (! (this.colliding(this.tank, this.leftEdge)
                     && v == Tank.LEFT)
                && 
                ! (this.colliding(this.tank, this.rightEdge)
                     && v == Tank.RIGHT)) {
            this.tank.setVelocity(v.vx, v.vy);
        }
    }


    /**
     * Tell the tank to fire and add the resulting bullet to the model
     */
    public void tankShoot() {
        Bullet bullet = this.tank.shoot();
        this.addGameObject(bullet);
    }



    // game state setters and getters
    public void pause() {
        this.paused = true;
    }
    public void unpause() {
        this.paused = false;
    }
    public boolean isPaused() {
        return this.paused;
    }
    private void gameOver() {
        this.gameComplete = true;
        this.playerWon = false;
        this.pause();
    }
    private void gameWon() {
        this.gameComplete = true;
        this.playerWon = true;
        this.pause();
    }
    public boolean isComplete() {
        return this.gameComplete;
    }
    public boolean playerWon() {
        return this.playerWon;
    }

    
    // other getters and setters
    public ArrayList<GameObject> getState() {
        return this.objects;
    }
    public int getScore() {
        return this.score;
    }
    public void resetScore() {
        this.score = 0;
    }

}
