package spaceinvaders.src;

import java.awt.event.*;

import spaceinvaders.src.models.*;
import spaceinvaders.src.objects.active.*;

/**
 * SpaceInvaders.java
 * @author Denis Griffis
 *
 * The master Controller for the Space Invaders application (constructed in the
 * Model-View-Controller pattern).
 *
 * This class is responsible for two things:
 *    1) Processing all user input (keyboard-only), and manipulating the models
 *       internal gamestates in response;
 *    2) Timing control; alternately waiting and stepping the internal models
 *       and the associated View.
 *
 * @see View.java
 * @see SpaceInvadersModel.java
 * @see HighScoresModel.java
 */
public class SpaceInvaders implements KeyListener {

    private int gamestate;
    private int priorGamestate;
    private View view;
    private SpaceInvadersModel game;
    private HighScoresModel highscores;

    private static int WELCOME = 0;
    private static int GAME = 1;
    private static int HIGHSCORES = 2;
    
    public static void main(String[] args) {
        SpaceInvaders spaceinvaders = new SpaceInvaders();
        spaceinvaders.run();
    }


    /**
     * Initialize the necessary View and Models
     */
    public SpaceInvaders() {
        this.gamestate = SpaceInvaders.WELCOME;

        this.game = new SpaceInvadersModel(800, 600);
        this.highscores = new HighScoresModel();

        this.view = new View(View.WELCOME, 800, 600, this.game,
                             this.highscores);
        this.view.addKeyListener(this);
    }


    /**
     * Run the controller until the window is closed
     */
    public void run() {

        // these are for delaying switching game modes on the game being won or
        // lost
        int updateDelay = 0;
        int updateDelayCount = 0;

        while (true) {
            try {
                //Thread.sleep(2000);
                Thread.sleep(20);
            } catch (Exception e) {
            }

            // only the actual game requires timestep processing by the
            // Controller
            if (this.gamestate == SpaceInvaders.GAME) {
                this.game.step();

                if (this.game.isComplete() && updateDelay == 0) {
                    if (this.game.playerWon()) {
                        updateDelay = 55;
                        this.view.displayBigMessage(" You won! ");
                    } else {
                        updateDelay = 100;
                        this.view.displayBigMessage("Game over!");
                    }
                } else if (this.game.isComplete()) {
                    updateDelayCount++;
                    if (updateDelayCount == updateDelay) {
                        updateDelay = 0;
                        updateDelayCount = 0;
                        this.view.resetBigMessage();
                        if (this.game.playerWon()) {
                            this.game.reinitialize();
                        } else {
                            int score = this.game.getScore();
                            if (this.highscores.isItHigh(score)) {
                                this.highscores.askForInitials(score);
                            }
                            this.gamestate = SpaceInvaders.HIGHSCORES;
                            this.view.setScreen(View.HIGHSCORES);
                        }
                    }
                }
            }

            // and always step the view
            this.view.update();
        }
    }


    /**
     * Handle any and all key press events, including:
     * - starting the player's tank moving left or right
     */
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            // left arrow
            case 37:
                if (this.gamestate == SpaceInvaders.GAME) {
                    this.game.moveTank(Tank.LEFT);
                }
                break;

            // right arrow
            case 39:
                if (this.gamestate == SpaceInvaders.GAME) {
                    this.game.moveTank(Tank.RIGHT);
                }
                break;
        }
    }


    /**
     * Handle any and all key release events, including:
     * - stopping the player's tank
     */
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case 37:
            case 39:
                if (this.gamestate == SpaceInvaders.GAME) {
                    this.game.moveTank(Tank.STOP);
                }
                break;
        }
    }


    /**
     * Handle any and all events dependent only on a key press:
     * - switching game modes
     * - firing player gun
     * - resetting game
     * - pausing game
     * - recording player initials for high scores
     */
    public void keyTyped(KeyEvent e) {

        char typedKey = e.getKeyChar();

        switch (typedKey) {

            // <Enter> pressed; switches from the welcome screen to the game
            // mode
            case '\n':
                if (this.gamestate == SpaceInvaders.WELCOME) {
                    // reinitialize the game
                    this.game.reinitialize();
                    this.game.resetScore();
                    // and switch to game mode
                    this.gamestate = SpaceInvaders.GAME;
                    this.view.setScreen(View.GAME);
                }
                break;


            // <Space> pressed; if in-game, fires the tank's gun
            case ' ':
                if (this.gamestate == SpaceInvaders.GAME) {
                    this.game.tankShoot();
                }
                break;


            // P pressed; pauses the game and displays the high scores
            case 'p': case 'P':
                // if the HighScoresModel is recording a player's initials,
                // that overrides everything else
                if (this.gamestate == SpaceInvaders.HIGHSCORES
                        && this.highscores.isRecording()) {
                    typedKey = Character.toUpperCase(typedKey);
                    this.highscores.recordLetter(typedKey);
                    break;
                }

                if (this.gamestate != SpaceInvaders.HIGHSCORES) {
                    if (this.gamestate == SpaceInvaders.GAME) {
                        this.game.pause();
                    }
                    this.priorGamestate = this.gamestate;
                    this.gamestate = SpaceInvaders.HIGHSCORES;
                    this.view.setScreen(View.HIGHSCORES);
                } else {
                    // only enable switching back to the game if it has not yet
                    // been completed
                    if (! this.game.isComplete()) {
                        this.gamestate = this.priorGamestate;
                    } else {
                        this.gamestate = SpaceInvaders.WELCOME;
                    }
                    this.view.setScreen(
                        (this.gamestate == SpaceInvaders.GAME) ?
                        View.GAME : View.WELCOME);
                }
                break;


            // unannounced hotkeys
            case 'r': case 'R':
                // if the HighScoresModel is recording a player's initials,
                // that overrides everything else
                if (this.gamestate == SpaceInvaders.HIGHSCORES
                        && this.highscores.isRecording()) {
                    typedKey = Character.toUpperCase(typedKey);
                    this.highscores.recordLetter(typedKey);
                    break;
                }

                // otherwise, reset the game
                if (this.gamestate == SpaceInvaders.GAME) {
                    this.game.reinitialize();
                    this.game.resetScore();
                }
                break;

            case 't': case 'T':
                // if the HighScoresModel is recording a player's initials,
                // that overrides everything else
                if (this.gamestate == SpaceInvaders.HIGHSCORES
                        && this.highscores.isRecording()) {
                    typedKey = Character.toUpperCase(typedKey);
                    this.highscores.recordLetter(typedKey);
                    break;
                }

                // otherwise, toggle game paused
                if (this.gamestate == SpaceInvaders.GAME) {
                    if (this.game.isPaused()) { this.game.unpause(); }
                    else { this.game.pause(); }
                }
                break;


            // any other letters just go to the high scores model, if it's
            // recording player initials
            case 'a': case 'b': case 'c': case 'd': case 'e':
            case 'f': case 'g': case 'h': case 'i': case 'j':
            case 'k': case 'l': case 'm': case 'n': case 'o':
            case 'q': case 's': case 'u': case 'v': case 'w':
            case 'x': case 'y': case 'z':
                typedKey = Character.toUpperCase(typedKey);
            case 'A': case 'B': case 'C': case 'D': case 'E':
            case 'F': case 'G': case 'H': case 'I': case 'J':
            case 'K': case 'L': case 'M': case 'N': case 'O':
            case 'Q': case 'S': case 'U': case 'V': case 'W':
            case 'X': case 'Y': case 'Z':
                if (this.gamestate == SpaceInvaders.HIGHSCORES
                        && this.highscores.isRecording()) {
                    this.highscores.recordLetter(typedKey);
                }
                break;

        }
    }

}
