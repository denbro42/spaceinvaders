package spaceinvaders.src.objects.stationary;

import spaceinvaders.src.objects.GameObject;

/**
 * StationaryObject.java
 * @author Denis Griffis
 *
 * Parent class of all non-moving objects in the game; no common functionality,
 * but gives ability of all stationary objects to be treated commonly
 */
public abstract class StationaryObject extends GameObject {

    public StationaryObject(int x, int y) {
        super(x,y);
    }

}
