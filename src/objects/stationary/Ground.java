package spaceinvaders.src.objects.stationary;

import spaceinvaders.src.animators.*;

/**
 * Ground.java
 * @author Denis Griffis
 *
 * The ground.
 * Ground has no functionality by itself, but provides collision cases for
 * object deletion/victory conditions
 */
public class Ground extends StationaryObject {

    public Ground(int x, int y) {
        super(x, y);
        this.setAnimator(new GroundAnimator(this));
    }

}
