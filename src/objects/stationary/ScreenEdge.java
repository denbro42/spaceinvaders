package spaceinvaders.src.objects.stationary;

import spaceinvaders.src.animators.*;


/**
 * ScreenEdge.java
 * @author Denis Griffis
 *
 * The edge of the game screen/world; no direct functionality, but provides
 * collision cases for movement control/object deletion
 */
public class ScreenEdge extends StationaryObject {

    public static int LEFT = 0;
    public static int TOP = 1;
    public static int RIGHT = 2;

    public ScreenEdge(int side, int screenWidth, int screenHeight) {
        super(0, 0);
        //this.setAnimator(new ScreenEdgeAnimator(this));
        this.width = screenWidth;
        this.height = screenHeight;
        if (side == ScreenEdge.LEFT) {
            this.positionX = -screenWidth + 1;
            this.positionY = 0;
        } else if (side == ScreenEdge.TOP) {
            this.positionX = 0;
            this.positionY = -screenHeight + 1;
        } else if (side == ScreenEdge.RIGHT) {
            this.positionX = screenWidth - 1;
            this.positionY = 0;
        }
    }
}
