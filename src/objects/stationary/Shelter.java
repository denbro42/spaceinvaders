package spaceinvaders.src.objects.stationary;

import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.objects.active.*;
import spaceinvaders.src.animators.*;


/**
 * Shelter.java
 * @author Denis Griffis
 *
 * The bullet-absorbing Shelter; the only object in the game with health
 * (gasp!)
 */
public class Shelter extends StationaryObject {

    private int damageLimit = 3;
    private int damageCount;

    public Shelter(int x, int y) {
        super(x, y);
        this.damageCount = 0;
        this.setAnimator(new ShelterAnimator(this));
    }

    public int collision(GameObject obj) {
        if (obj instanceof Bullet) {
            Bullet bullet = (Bullet)obj;
            return this.collision(bullet);
        }
        else if (obj instanceof Enemy) {
            Enemy enemy = (Enemy)obj;
            return this.collision(enemy);
        }
        return GameObject.NOACTION;
    }
    // collision cases
    private int collision(Bullet bullet) {
        this.damageCount++;
        if (this.damageCount >= this.damageLimit) {
            return GameObject.DELETE;
        } else {
            ShelterAnimator anim = (ShelterAnimator)this.animator;
            anim.showDamage(this.damageCount);
        }
        return GameObject.NOACTION;
    }
    // Enemies are serious weapons of destruction
    private int collision(Enemy enemy) {
        return GameObject.DELETE;
    }

}
