package spaceinvaders.src.objects.active;

import spaceinvaders.src.animators.*;
import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.objects.stationary.*;


/**
 * UFO.java
 * @author Denis Griffis
 *
 * The randomly-appearing high-yield always-elusive UFO
 */
public class UFO extends ActiveObject {

    private int score = 500;

    public UFO(int x, int y, int vx, int vy) {
        super(x,y,vx,vy);
        this.setAnimator(new UFOAnimator(this));

        this.deathSteps = 25;
    }

    public int collision(GameObject obj) {
        if (obj instanceof Bullet) {
            Bullet b = (Bullet)obj;
            return this.collision(b);
        }
        if (obj instanceof ScreenEdge) {
            ScreenEdge edge = (ScreenEdge)obj;
            return this.collision(edge);
        }
        return GameObject.NOACTION;
    }
    // collision cases
    private int collision(Bullet bullet) {
        if (bullet.isPlayerBullet()) {
            UFOAnimator anim = (UFOAnimator)this.animator;
            anim.explode();
            return GameObject.MARKFORDEATH;
        }
        return GameObject.NOACTION;
    }
    private int collision(ScreenEdge edge) {
        return GameObject.DELETE;
    }


    // getter
    public int getScore() {
        return this.score;
    }
}
