package spaceinvaders.src.objects.active;

import spaceinvaders.src.animators.*;
import spaceinvaders.src.models.Velocity;
import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.objects.stationary.*;


/**
 * Enemy.java
 * @author Denis Griffis
 *
 * Class providing all functionality for Drones, Hunters, and Squids; these
 * enemies are similar enough that they do not need separate classes, so cased
 * behavior is provided here
 *
 * @see SpaceInvadersModel.java
 */
public class Enemy extends ActiveObject {

    private int type;
    public static int DRONE = 0;
    public static int HUNTER = 1;
    public static int SQUID = 2;

    // velocity of any Enemy-generated bullets
    private static Velocity bulletVel = new Velocity(0, 5);

    // interaction behavior
    private int score = 100;
    private int shiftVelocity = 2;

    public Enemy(int x, int y, int vx, int vy, int type) {
        super(x, y, vx, vy);
        this.type = type;

        // game steps after exploding before this is deleted
        this.deathSteps = 15;

        this.setAnimator(new EnemyAnimator(this, this.getType()));
    }


    public Bullet shoot() {
        return new Bullet(this.positionX + (this.width/2),
                          this.positionY + this.height,
                          Enemy.bulletVel.vx, Enemy.bulletVel.vy, false);
    }


    public int collision(GameObject obj) {
        if (obj instanceof Bullet) {
            Bullet b = (Bullet)obj;
            return this.collision(b);
        }
        else if (obj instanceof ScreenEdge) {
            ScreenEdge edge = (ScreenEdge)obj;
            return this.collision(edge);
        }
        else if (obj instanceof Ground) {
            Ground ground = (Ground)obj;
            return this.collision(ground);
        }
        return GameObject.NOACTION;
    }
    // collision cases
    private int collision(Bullet bullet) {
        if (bullet.isPlayerBullet()) {
            EnemyAnimator anim = (EnemyAnimator)this.animator;
            anim.explode();
            return GameObject.MARKFORDEATH;
        }
        return GameObject.NOACTION;
    }
    private int collision(ScreenEdge edge) {
        return GameObject.REVERSE;
    }
    private int collision(Ground ground) {
        return GameObject.GAMELOST;
    }


    // motion-handling functions
    public void reverse() {
        this.setVelocity(-this.getXVelocity(), this.getYVelocity());
    }
    public void startShiftingDown() {
        this.setVelocity(this.getXVelocity(), this.shiftVelocity);
    }
    public void stopShiftingDown() {
        this.setVelocity(this.getXVelocity(), 0);
    }


    // getters
    public int getScore() {
        return this.score;
    }
    public int getType() {
        return this.type;
    }

}
