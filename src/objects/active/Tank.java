package spaceinvaders.src.objects.active;

import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.objects.stationary.*;
import spaceinvaders.src.animators.*;
import spaceinvaders.src.models.Velocity;

/**
 * Tank.java
 * @author Denis Griffis
 *
 * The Tank (player) object for Space Invaders.
 *
 * @see SpaceInvadersModel.java
 */
public class Tank extends ActiveObject {

    // hard-coded movement velocities, given here for readability
    public static Velocity STOP = new Velocity(0, 0);
    public static Velocity LEFT = new Velocity(-10, 0);
    public static Velocity RIGHT = new Velocity(10, 0);
    // the velocity of any player-produced bullets
    private static Velocity bulletVel = new Velocity(0, -10);

    public Tank(int x, int y, int vx, int vy) {
        super(x, y, vx, vy);
        this.setAnimator(new TankAnimator(this));

        this.deathSteps = 500;
    }

    public Bullet shoot() {
        return new Bullet(this.positionX + (this.width/2), this.positionY,
                          this.bulletVel.vx, this.bulletVel.vy, true);
    }


    public int collision(GameObject obj) {
        if (obj instanceof ScreenEdge) {
            ScreenEdge edge = (ScreenEdge)obj;
            return this.collision(edge);
        }
        else if (obj instanceof Bullet) {
            Bullet bullet = (Bullet)obj;
            return this.collision(bullet);
        }
        else if (obj instanceof Enemy) {
            Enemy enemy = (Enemy)obj;
            return this.collision(enemy);
        }
        return GameObject.NOACTION;
    }
    // collision cases
    private int collision(ScreenEdge edge) {
        this.setVelocity(Tank.STOP.vx, Tank.STOP.vy);
        return GameObject.NOACTION;
    }
    private int collision(Bullet bullet) {
        if (bullet.isPlayerBullet()) { return GameObject.NOACTION; }
        else {
            TankAnimator anim = (TankAnimator)this.animator;
            anim.explode();
            return GameObject.MARKFORDEATH;
        }
    }
    private int collision(Enemy enemy) {
        return GameObject.MARKFORDEATH;
    }


}
