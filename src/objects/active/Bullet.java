package spaceinvaders.src.objects.active;

import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.objects.stationary.*;
import spaceinvaders.src.animators.*;


/**
 * Bullet.java
 * @author Denis Griffis
 *
 * Common class for player/enemy projectiles
 *
 * @see Tank.java
 * @see Enemy.java
 * @see SpaceInvadersModel.java
 */
public class Bullet extends ActiveObject {

    // marks if this is a player- or enemy-fired bullet
    private boolean playerBullet;

    public Bullet(int x, int y, int vx, int vy, boolean playerfired) {
        super(x, y, vx, vy);
        this.playerBullet = playerfired;
        this.setAnimator(new BulletAnimator(this, playerfired));
    }

    public int collision(GameObject obj) {
        if (obj instanceof StationaryObject) {
            StationaryObject stationary = (StationaryObject)obj;
            return this.collision(stationary);
        }
        else if (obj instanceof Enemy) {
            Enemy en = (Enemy)obj;
            return this.collision(en);
        }
        else if (obj instanceof Tank) {
            Tank tank = (Tank)obj;
            return this.collision(tank);
        }

        return GameObject.NOACTION;
    }
    // collision cases
    private int collision(Enemy en) {
        if (this.isPlayerBullet()) {
            return GameObject.DELETE;
        } else {
            return GameObject.NOACTION;
        }
    }
    private int collision(Tank tank) {
        if (this.isPlayerBullet()) {
            return GameObject.NOACTION;
        } else {
            return GameObject.DELETE;
        }
    }
    private int collision(StationaryObject stationary) {
        return GameObject.DELETE;
    }


    // getter
    public boolean isPlayerBullet() {
        return this.playerBullet;
    }

}
