package spaceinvaders.src.objects.active;

import spaceinvaders.src.objects.GameObject;


/**
 * ActiveObject.java
 * @author Denis Griffis
 *
 * Abstract parent of all movable objects in the game; adds velocity
 * functionality to basic GameObject functionality
 */
public abstract class ActiveObject extends GameObject {

    protected int velocityX;
    protected int velocityY;

    public ActiveObject(int x, int y, int vx, int vy) {
        super(x,y);
        this.velocityX = vx;
        this.velocityY = vy;
    }

    public void step() {
        super.step();
        this.positionX += this.velocityX;
        this.positionY += this.velocityY;
    }

    public int getXVelocity() {
        return this.velocityX;
    }
    public int getYVelocity() {
        return this.velocityY;
    }
    public void setVelocity(int vx, int vy) {
        this.velocityX = vx;
        this.velocityY = vy;
    }
}
