package spaceinvaders.src.objects;

import javax.swing.*;
import java.awt.*;

import spaceinvaders.src.animators.*;

/**
 * GameObject.java
 * @author Denis Griffis
 * 
 * The abstract parent for all objects in the game; provides basic common
 * functionality for interfacing with other parts of the codebase
 *
 * @see SpaceInvadersModel.java
 */
public abstract class GameObject {

    protected int positionX;
    protected int positionY;

    // all visible objects require an Animator to draw them
    protected Animator animator = null;
    protected boolean visible = false;
    protected int width;
    protected int height;

    // return values for collisions
    public static int NOACTION = 0;
    public static int DELETE = 1;
    public static int MARKFORDEATH = 2;
    public static int REVERSE = 3;
    public static int GAMELOST = 4;

    // values for marking for death behavior (explosions, etc.)
    protected boolean dying = false;
    protected int deathSteps = 0;
    protected int deathStepCount = 0;

    /**
     * Basic initialization; set the initial position and velocity
     */
    public GameObject(int x, int y) {
        this.positionX = x;
        this.positionY = y;
    }

    public int collision(GameObject other) { return GameObject.NOACTION; }


    // slow death (delayed delete) functions
    public void markForDeath() { this.dying = true; }
    public boolean isDying() { return this.dying; }
    public boolean readyToDie() {
        return this.deathStepCount >= this.deathSteps;
    }


    /**
     * Adjust the object's position by its velocity values, and step its
     * sprite's frame if the object is visible
     */
    public void step() {
        if (this.visible) {
            this.animator.frameStep();
        }
        if (this.dying) {
            this.deathStepCount++;
        }
    }

    /**
     * Render the GameObject's sprite at its current position if visible
     */
    public void draw(Graphics g, JPanel observer) {
        if (this.visible) {
            this.animator.draw(g, this.getX(), this.getY(), observer);
        }
    }

    /* accessors */
    public int getX() {
        return this.positionX;
    }
    public int getY() {
        return this.positionY;
    }
    public int getWidth() {
        return this.width;
    }
    public int getHeight() {
        return this.height;
    }

    public void setAnimator(Animator anim) {
        this.animator = anim;
        this.visible = true;
        this.updateDimensions();
    }

    public void updateDimensions() {
        this.width = this.animator.getCurrentWidth();
        this.height = this.animator.getCurrentHeight();
    }

}

