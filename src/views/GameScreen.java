package spaceinvaders.src.views;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import spaceinvaders.src.objects.*;

/**
 * GameScreen.java
 * @author Denis Griffis
 *
 * JPanel subclass responsible for rendering the Space Invaders game screen,
 * including player and enemy objects, the world, and the score.
 *
 * @see View.java
 * @see SpaceInvadersModel.java
 */
public class GameScreen extends Screen {

    private ArrayList<GameObject> objects;
    private int score = 0;

    public GameScreen(Dimension d, Color c) {
        super(d, c);
        this.objects = new ArrayList<GameObject>();
    }

    public void updateObjects(ArrayList<GameObject> objs) {
        this.objects = objs;
    }

    public void displayScore(int s) {
        this.score = s;
    }


    /**
     * Re-render the panel, including the background and all of its GameObjects
     */
    public void paint(Graphics g) {
        // Fill the panel with the background color.  That is,
        // erase the whole thing.
        Dimension size = getSize();
        g.setColor(getBackground());
        g.fillRect(0, 0, size.width, size.height);

        // draw each of this panel's GameObjects
        for (int i=0; i<this.objects.size(); i++) {
            GameObject obj = this.objects.get(i);
            obj.draw(g, this);
        }

        // draw the score
        g.setColor(Color.white);
        g.setFont(new Font("SansSerif", Font.PLAIN, 35));
        g.drawString("Score:", 30, 40);
        g.drawString(Integer.toString(this.score), 150, 40);

        // if there's a bigMessage to display, show it!
        if (this.bigMessage.length() > 0) {
            g.setColor(new Color(255, 215, 0));
            g.setFont(new Font("Monospaced", Font.BOLD, 120));
            g.drawString(this.bigMessage, 50, 275);
        }

        // print the controls
        g.setColor(Color.white);
        g.setFont(new Font("Monospaced", Font.PLAIN, 14));
        g.drawString("Controls:    <- : Move left  |"
                     +"  -> : Move right  |  Space : "
                     +"Fire  |  P : Show High Scores", 40, 590);
    }
}
