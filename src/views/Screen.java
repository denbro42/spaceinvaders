package spaceinvaders.src.views;

import java.awt.*;
import javax.swing.*;
import java.util.*;

import spaceinvaders.src.objects.GameObject;
import spaceinvaders.src.models.Score;


/**
 * Screen.java
 * @author Denis Griffis
 *
 * Abstract parent (extending JPanel) providing common functionality for Space
 * Invaders screens
 */
public abstract class Screen extends JPanel {

    protected String bigMessage = "";

    public Screen(Dimension d, Color color) {
        super();
        this.setPreferredSize(d);
        this.setBackground(color);
    }

    public abstract void paint(Graphics g);

    public void displayBigMessage(String message) {
        this.bigMessage = message;
    }
    public void resetBigMessage() {
        this.bigMessage = "";
    }

}
