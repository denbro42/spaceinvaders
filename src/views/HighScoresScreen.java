package spaceinvaders.src.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import spaceinvaders.src.models.Score;
import spaceinvaders.src.objects.active.*;


/**
 * HighScoresScreen.java
 * @author Denis Griffis
 *
 * Responsible for rendering the High Scores Screen, including scores and
 * decorative game objects
 *
 * @see View.java
 * @see HighScoresModel.java
 */
public class HighScoresScreen extends Screen {

    private Score[] highscores;

    // decoration objects
    private Enemy squid;
    private Enemy hunter;
    private Enemy drone;
    private UFO ufo;
    private Tank tank;

    public HighScoresScreen(Dimension d, Color c) {
        super(d, c);
        this.highscores = new Score[0];

        // initialize the decorations
        this.squid = new Enemy(670, 150, 0, 0, Enemy.SQUID);
        this.hunter = new Enemy(670, 300, 0, 0, Enemy.HUNTER);
        this.drone = new Enemy(670, 450, 0, 0, Enemy.DRONE);
        this.ufo = new UFO(60, 150, 0, 0);
        this.tank = new Tank(62, 450, 0, 0);
    }


    /**
     * Update the list of Scores to display
     */
    public void updateHighScores(Score[] scores) {
        this.highscores = scores;
    }

    
    /**
     * Animate the decoration objects
     */
    private void stepDecorations() {
        this.squid.step();
        this.hunter.step();
        this.drone.step();
        this.ufo.step();
        this.tank.step();
    }


    /**
     * Render the high scores screen
     */
    public void paint(Graphics g) {
        Dimension d = this.getSize();
        g.setColor(this.getBackground());
        g.fillRect(0, 0, d.width, d.height);

        // Title 
        g.setColor(Color.white);
        g.setFont(new Font("SansSerif", Font.BOLD, 60));
        g.drawString("High Scores", 200, 150);

        // Scores
        g.setFont(new Font("Monospaced", Font.PLAIN, 30));
        for (int i=0; i<this.highscores.length; i++) {
            Score score = this.highscores[i];
            g.drawString(score.player + "............." + score.score, 215,
                            250 + i*50);
        }

        // Controls message
        g.setFont(new Font("SansSerif", Font.ITALIC, 20));
        g.drawString("To return to the main screen, press P", 220, 550);

        // decorations
        this.stepDecorations();
        this.squid.draw(g, this);
        this.hunter.draw(g, this);
        this.drone.draw(g, this);
        this.ufo.draw(g, this);
        this.tank.draw(g, this);
    }

}
