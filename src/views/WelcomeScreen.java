package spaceinvaders.src.views;

import javax.swing.*;
import javax.imageio.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

import spaceinvaders.src.objects.active.*;


/**
 * WelcomeScreen.java
 * @author Denis Griffis
 *
 * Responsible for rendering the start-up welcome screen
 *
 * @see View.java
 */
public class WelcomeScreen extends Screen {

    // used in drawing the flashing "Press Enter" message
    private int flashStepDelay = 50;
    private int flashStepCount;
    private boolean drawPressEnter;

    // decorative game objects
    private UFO ufo1, ufo2;


    public WelcomeScreen(Dimension d, Color c) {
        super(d, c);
        this.flashStepCount = 0;
        this.drawPressEnter = true;

        this.ufo1 = new UFO(80, 140, 0, 0);
        this.ufo2 = new UFO(680, 140, 0, 0);
    }


    public void paint(Graphics g) {
        Dimension d = this.getSize();
        g.setColor(this.getBackground());
        g.fillRect(0, 0, d.width, d.height);

        // draw the title text
        BufferedImage title = null;
        try {
            title = ImageIO.read(WelcomeScreen.class.getResource(
                                                    "/imgs/logo.jpg"));
        } catch (IOException e) { }
        g.drawImage(title, 240, 40, this);

        // set the color for all other text
        g.setColor(Color.white);

        // flash the "Press Enter" message
        this.flashStepCount++;
        if (this.flashStepCount >= this.flashStepDelay) {
            this.flashStepCount = 0;
            this.drawPressEnter = ! this.drawPressEnter;
        }
        // Press <Enter> to begin
        if (this.drawPressEnter) {
            g.setFont(new Font("SansSerif", Font.ITALIC, 30));
            g.drawString("Press <Enter>", 310, 370);
            g.drawString("To Begin", 347, 395);
        }

        // draw the controls
        g.setFont(new Font("Monospaced", Font.PLAIN, 15));
        g.drawString("Controls", 25, 475);
        g.drawString("<- (Left Arrow)   :   Move Left", 25, 500);
        g.drawString("-> (Right Arrow)  :   Move Right", 25, 525);

        // draw the high score message
        g.drawString("To see high scores", 620, 485);
        g.drawString("Press P", 670, 510);

        // draw the UFOs
        this.ufo1.step();
        this.ufo2.step();
        this.ufo1.draw(g, this);
        this.ufo2.draw(g, this);
    }

}
