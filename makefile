all:
	javac -sourcepath ../ -d bin/ src/*.java
clean:
	rm -rf bin/spaceinvaders
	rm -f bin/highscores.txt
test:
	make clean;
	make;
	java -cp bin/ spaceinvaders.src.SpaceInvaders
